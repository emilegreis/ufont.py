# futil

utils for (web) fonts

![futil](snapshots/2023-03-09_14-18-30.png "futil")

## Requirements

- fontforge

## Install

create venv, activate venv, install requirements

    python -m venv env
    source env/bin/activate
    pip install -r requirements.txt

## Use

Convert otf to woff2, woff and eot to freefont dir.

    ./futil convert -i freefont-20120503/Free*.otf -o woff2 woff eot otf -d freefont

Create css fontfaces (-o defines the fontfaces css "src url()" sequence).

    ./futil fontfaces -i freefont/Free* -o woff2 woff eot otf > freefont/fontfaces.css

Create a new directory containing the font files converted to the specified formats, a fontfaces.css file and a basic HTML specimen.

    ./futil specimen -i freefont-20120503/Free*.otf -o woff2 otf -d freefont-specimen

## License

GNU Affero General Public License v3.0. 
