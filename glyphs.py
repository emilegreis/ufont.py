#!/usr/bin/env python

# basic-latin
for i in range(32, 126):
    print('<span>&#'+str(i)+';</span>')

# latin-1 supplement
for i in range(160, 255):
    print('<span>&#'+str(i)+';</span>')
